<?php
	namespace classes ;

	/**
	* Арифметические действия с комплексными числами
	*/
	class Complex {
		/**
		* @var float $x - реальная часть
		*/
		public $x = 0.0 ;

		/**
		* @var float $i - мнимая часть
		*/
		public $i = 0.0 ;

		/**
		* Конструктор
		*
		* @param float $x - реальная часть
		* @param float $i - мнимая часть
		*/
		public function __construct( float $x = 0.0 , float $i = 0.0 ) {
			list( $this->x , $this->i ) = [ $x , $i , ] ;
		}

		/**
		* Сложение
		*
		* @param Complex $b - слагаемое
		*
		* @return Complex
		*/
		public function add( Complex $b ) : Complex {
			return new static( $this->x + $b->x , $this->i + $b->i ) ;
		}

		/**
		* Вычитание
		*
		* @param Complex $b - слагаемое
		*
		* @return Complex
		*/
		public function sub( Complex $b ) : Complex {
			return new static( $this->x - $b->x , $this->i - $b->i ) ;
		}

		/**
		* Умножение
		*
		* @param Complex $b - множитель
		*
		* @return Complex
		*/
		public function mul( Complex $b ) : Complex {
			return new static( $this->x * $b->x - $this->i * $b->i , $this->x * $b->i + $this->i * $b->x ) ;
		}

		/**
		* Деление
		*
		* @param Complex $b - делитель
		*
		* @return Complex
		*/
		public function div( Complex $b ) : Complex {
			$div = $b->x * $b->x + $b->i * $b->i ;

			return new static( ( $this->x * $b->x + $this->i * $b->i ) / $div , ( $this->i * $b->x - $this->x * $b->i ) / $div ) ;
		}

		/**
		* Преобразование к строке
		*
		* @return string
		*/
		public function __toString( ) {
			return '[' . implode( ',' , [ $this->x , $this->i , ] ) . ']' ;
		}
	}