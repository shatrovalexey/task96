<?php
	include_once( 'vendor/autoload.php' ) ;

	$test = new tests\classes\ComplexClassTest( ) ;

	foreach ( [ 'add' , 'sub' , 'mul' , 'div' , ] as $method ) {
		echo "$method\n" ;

		$test->$method( ) ;
	}