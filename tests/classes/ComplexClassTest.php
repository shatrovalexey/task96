<?php
	namespace tests\classes ;

	use PHPUnit\Framework\TestCase ;
	use classes\Complex as ComplexClass ;

class ComplexClassTest extends TestCase {
	/**
	* Сложение
	*/
	public function add( ) {
		$a = new ComplexClass( 10 , 20 ) ;
		$b = new ComplexClass( 20 , 10 ) ;

		$c = $a->add( $b ) ;

		$this->assertSame( '[30,30]' , "$c" ) ;
	}

	/**
	* Вычитание
	* @depends add
	*/
	public function sub( ) {
		$a = new ComplexClass( 10 , 20 ) ;
		$b = new ComplexClass( 20 , 10 ) ;

		$c = $a->sub( $b ) ;

		$this->assertSame( '[-10,10]' , "$c" ) ;
	}

	/**
	* Умножение
	*
	* @depends sub
	*/
	public function mul( ) {
		$a = new ComplexClass( 10 , 20 ) ;
		$b = new ComplexClass( 20 , 10 ) ;

		$c = $a->mul( $b ) ;

		$this->assertSame( '[0,500]' , "$c" ) ;
	}

	/**
	* Деление
	*
	* @depends mul
	*/
	public function div( ) {
		$a = new ComplexClass( 10 , 20 ) ;
		$b = new ComplexClass( 20 , 10 ) ;

		$c = $a->div( $b ) ;

		$this->assertSame( '[0.8,0.6]' , "$c" ) ;
	}
}